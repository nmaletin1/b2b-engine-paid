$(window).on('load resize', function() {
  var headerHeight = $('header').outerHeight();
  document.documentElement.style.setProperty('--header-height', headerHeight + 'px');
  var windowHeight = $(window).height();
});


$(function() {
	
  var headerHeight = $('header').outerHeight();
  var windowHeight = $(window).height();
  
  
  /* Header Position */
  function headerPosition(headerHeight) {
    if ( $(document).scrollTop() > headerHeight ) {
      $('header:not(.header-static)').addClass('reactive');
    }
    else {
      $('header:not(.header-static)').removeClass('reactive');
    }
  }

  var lastScrolledFromTop = headerHeight;
  
  $(window).on('scroll', function() {
    headerPosition(headerHeight);
    
    var scrolledFromTop = $(this).scrollTop();
    
    if ( scrolledFromTop > lastScrolledFromTop ) {
      /*down scroll*/
      if ($('#progress-wrapper').length) {
        $('.header-reactive').css('margin-top', '-' + ( parseInt(headerHeight) - 5 ) + 'px');
      } else {
        $('.header-reactive').css('margin-top', '-' + parseInt(headerHeight) + 'px');
      }
    } else {
      /*up scroll*/
      $('.header-reactive').css('margin-top', '0');
    }
    
    if ( $(document).scrollTop() > headerHeight ) {
      lastScrolledFromTop = scrolledFromTop;
    }
    
    if ( scrolledFromTop > windowHeight ) {
      $('.footer-scroll-to-top').addClass('footer-scroll-to-top-active');
    } else {
      $('.footer-scroll-to-top').removeClass('footer-scroll-to-top-active');
    }
  });
  

  /* Header Search and Language Switcher */
  if ( $('.header-language-switcher-wrapper').length ) {
    $('.header-language-switcher').appendTo('.header-language-switcher-wrapper');
  }
  
  if ( $('.header-main-container').hasClass('full-width') ) {
    $('.header-search-inner').addClass('full-width');    
  }
  
  if ( $('.header-search--pop-up').length ) {
    $('.header-search-open').appendTo('.header-search-wrapper');
    $('.header-search-open').removeClass('hidden');
    $('.header-search-close').css('top', $('.header-search-open').offset().top - $(window).scrollTop());
    //$('.header-search-close').css('right', $(window).width() - ($('.header-search-open').offset().left + $('.header-search-open').outerWidth()));
  } else if ( $('.header-search--inline').length ) {
    $('.header-search').appendTo('.header-search-wrapper');
    if ( $('.header-search--slide-in').length ) {
      $('.header-search-open').appendTo('.header-search-wrapper');
      $('.header-search-close').appendTo('.header-search-wrapper');
      $('.header-search-open').removeClass('hidden');
      $('.header-search-close').addClass('hidden');
    }
  }

	$('.header-search-open').on('click', function() {
		$('.header-search').addClass('header-search-active');
    if ( $('.header-search--inline').length ) {
      $('.header-search-open').addClass('hidden');
      $('.header-search-close').removeClass('hidden');
    }
    return false;
	});
  
	$('.header-search-close').on('click', function() {
    $('.header-search').removeClass('header-search-active');
    if ( $('.header-search--inline').length ) {
      $('.header-search-open').removeClass('hidden');
      $('.header-search-close').addClass('hidden');
    }
    return false;
	});
  
  
  /* AOS */
  /*if ( !$('html').hasClass('hs-inline-edit') ) {*/
  AOS.init({
    // Global settings:
    disable:
      {% if request.postDict.inpageEditorUI %}
        true
      {% elif theme.animations.enable_animations and theme.animations.disable_animations == "0" %}
        false
      {% elif theme.animations.enable_animations and theme.animations.disable_animations != "0" %}
        function() {
          return window.innerWidth < {{ theme.animations.disable_animations }}; 
        }
      {% else %}
        true
      {% endif %}
    , // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
    startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
    initClassName: 'aos-init', // class applied after initialization
    animatedClassName: 'aos-animate', // class applied on animation
    useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
    disableMutationObserver: false, // disables automatic mutations' detections (advanced)
    debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
    throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
    // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
    offset: 0, // offset (in px) from the original trigger point
    delay: {{ theme.animations.animation_delay }}, // values from 0 to 3000, with step 50ms
    duration: {{ theme.animations.animation_duration }}, // values from 0 to 3000, with step 50ms
    easing: 'ease', // default easing for AOS animations
    once: true, // whether animation should happen only once - while scrolling down
    mirror: false, // whether elements should animate out while scrolling past them
    anchorPlacement: 'center', // defines which position of the element regarding to window should trigger the animation
  });
  /*};*/
  
  
  /* Scroll to Top */
	$('.footer-scroll-to-top').on('click', function() {
		$('html, body').animate({ scrollTop: 0 }, 'slow');
		return false;
	});
  
  
  /* Page Slug as Body Class */
  function addPageSlugBodyClass() {
    let pathArray = window.location.pathname.split('/');
    let pageSlug = pathArray.pop();
    if ( pageSlug == '' ) {
      pageSlug = 'home';
    }
    let pageSlugClass= 'page-' + pageSlug;
    $('body').addClass(pageSlugClass);
  };
  
  addPageSlugBodyClass();
  
  
  /* Scroll to Page Section */
	$('a[href*="#"]').not('[href="#"], [href^="#popup"]').on('click', function(event) {
    if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
      var target = $(this.hash);
      if ( target.length ) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top - headerHeight
        }, 500, function() {
          var $target = $(target);
          $target.focus();
          if ( $target.is(':focus') ) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
	});
  
  
	/* Mobile Menu */   
	$('.header-menu-mobile .hs-item-has-children > a').after('<div class="submenu-button">{% icon name="chevron-down" style="SOLID" unicode="f078", no_wrapper=True %}</div>');
	$('.menu-mobile-button').click(function() {
		$('.hs-menu-children-wrapper').hide();
    $('.header-menu-mobile').slideToggle(200);
		return false;
	});

	$('.submenu-button').click(function() {
		$(this).parent().siblings('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(200);
		$(this).next('.hs-menu-children-wrapper').slideToggle(200);
		$(this).next('.hs-menu-children-wrapper').children('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(200);
		return false;
	});
  

  /* Slick Slider */ 
  $('.slider-enabled').each(function() {
    var arrows = $(this).data('slider-arrows');
    var dots = $(this).data('slider-dots');
    var itemsInRow = $(this).data('slider-items');
    var autoplay =  $(this).data('slider-autoplay');
    var autoplaySpeed = ($(this).data('slider-autoplay_speed') || 4) * 1000;

    if ( itemsInRow == 1 ) {
      $(this).slick({
        arrows: arrows,
        dots: dots,
        slidesToShow: itemsInRow,
        slidesToScroll: 1,
        infinite: true,
        autoplay: autoplay,
        autoplaySpeed: autoplaySpeed,
        adaptiveHeight: false,
        responsive: [
          {
            breakpoint: {{ theme.spacing.max_width + 110 }},
            settings: {
              arrows: false
            }
          }
        ]
      });
    }
    else {
      $(this).slick({
        arrows: arrows,
        dots: dots,
        slidesToShow: itemsInRow,
        slidesToScroll: 1,
        infinite: true,
        autoplay: autoplay,
        autoplaySpeed: autoplaySpeed,
        adaptiveHeight: false,
        responsive: [
          {
            breakpoint: {{ theme.spacing.max_width + 110 }},
            settings: {
              arrows: false
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              arrows: false
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              arrows: false
            }
          }
        ]
      });
    }
  });
  
  
  /* Magnific Popup */
  $('.popup-enabled').magnificPopup({
    mainClass: 'mfp-with-zoom',
    fixedContentPos: true,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    closeMarkup: '<div title="%title%" class="mfp-close">{% icon name="times" style="SOLID" unicode="f00d", no_wrapper=True %}</div>',
    callbacks: {
      open: function() {
        $('body').addClass('popup-open');
      },
      close: function() {
        $('body').removeClass('popup-open');
      }
    },
    type: 'inline'
  });


  /* Magnific Popup Image */
  $('.popup-image-enabled').magnificPopup({
    mainClass: 'mfp-with-zoom',
    fixedContentPos: true,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    closeMarkup: '<div title="%title%" class="mfp-close">{% icon name="times" style="SOLID" unicode="f00d", no_wrapper=True %}</div>',
    callbacks: {
      open: function() {
        $('body').addClass('popup-open');
      },
      close: function() {
        $('body').removeClass('popup-open');
      }
    },
    type: 'image'
  });


  /* Magnific Popup Gallery */
  $('.popup-gallery-enabled').each(function() { // the containers for all your galleries
      $(this).magnificPopup({
        mainClass: 'mfp-with-zoom',
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        closeMarkup: '<div title="%title%" class="mfp-close">{% icon name="times" style="SOLID" unicode="f00d", no_wrapper=True %}</div>',
        callbacks: {
          open: function() {
            $('body').addClass('popup-open');
          },
          close: function() {
            $('body').removeClass('popup-open');
          }
        },
        type: 'image',
        delegate: '.popup-gallery-item', // the selector for gallery item
        gallery: {
          enabled: true,
          preload: [1,2], // 1 previous, 2 next
          navigateByImgClick: false,
          arrowMarkup: '<div title="%title%" class="mfp-arrow mfp-arrow-%dir%"></div>', // markup of an arrow button
          tPrev: 'Previous (Left arrow key)', // title for left button
          tNext: 'Next (Right arrow key)', // title for right button
          tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
        }
      });
  });


  /* Magnific Popup Video */
  $('.popup-video-enabled').magnificPopup({
    mainClass: 'mfp-with-zoom',
    fixedContentPos: true,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    closeMarkup: '<div title="%title%" class="mfp-close">{% icon name="times" style="SOLID" unicode="f00d", no_wrapper=True %}</div>',
    callbacks: {
      open: function() {
        $('body').addClass('popup-open');
      },
      close: function() {
        $('body').removeClass('popup-open');
        $('.popup-video').find('video').trigger('pause');
        $('.popup-video').find('iframe').attr('src', '');
      }
    },
    type: 'inline'
  });


  /* Blog Reading Progress Bar */
  function setProgress(headerHeight) {
    var start = document.querySelector('.blog-post').getBoundingClientRect().top;
    var height = document.querySelector('.blog-post').getBoundingClientRect().height;
    if ( start > 0 ) {
      $('#blog-progress-bar').css('width', 0);
    } else {
      $('#blog-progress-bar').css('width', Math.abs(start) / height * 100 + "%");
    }
    if ( $('.header-sticky').length ) {
      $('#blog-progress-wrapper').css('top', headerHeight);
    } else if ( $('.header-reactive').length ) {
      $('header').append($('#blog-progress-wrapper'));
    }
  }

  if ( $('.blog-post').length ) {
    $(window).on('scroll', function() {
      setProgress(headerHeight);
    });
  }


  /* Blog Sidebar Search */
  if ( $('.blog-index__sidebar-search').length ) {
    $('.blog-index__sidebar-search-inner').appendTo('.blog-index__sidebar-search');
  }


  /* Section Options */
  $('.section-options').closest('.row-fluid-wrapper').addClass('section-options-wrapper');
  

  /*Accordion Auto Toggle*/
  $('.accordion-auto-toggle .accordion-head').on('click', function() {
    if ( $(this).closest('.accordion-single').hasClass('active') ) {
      $(this).closest('.accordion-single').removeClass('active');
      $(this).siblings('.accordion-body').slideUp();
    } else {
      $(this).closest('.accordion').find('.accordion-single').removeClass('active');
      $(this).closest('.accordion-single').addClass('active');
      $(this).closest('.accordion').find('.accordion-single:not(.active) .accordion-body').slideUp();
      $(this).siblings('.accordion-body').slideDown();
    }
  });


  /*Accordion Manual Toggle*/
  $('.accordion-manual-toggle .accordion-head').on('click', function() {
    $(this).closest('.accordion-single').toggleClass('active');
    $(this).siblings('.accordion-body').slideToggle();
  });


  /*Career Listing Auto Toggle*/
  $('.accordion-auto-toggle .career-listing-accordion-toggle-inner').on('click', function() {
    if ( $(this).closest('.career-listing-single').hasClass('active') ) {
      $(this).closest('.career-listing-single').removeClass('active');
      $(this).parent().siblings('.career-listing-body-inner').slideUp();
      $(this).find('.career-listing-accordion-toggle-text-close').hide();
      $(this).find('.career-listing-accordion-toggle-text-open').show();
    } else {
      $(this).closest('.career-listing').find('.career-listing-single').removeClass('active');
      $(this).closest('.career-listing-single').addClass('active');
      $(this).closest('.career-listing').find('.career-listing-single:not(.active) .career-listing-body-inner').slideUp();
      $(this).closest('.career-listing').find('.career-listing-single:not(.active) .career-listing-accordion-toggle-text-close').hide();
      $(this).closest('.career-listing').find('.career-listing-single:not(.active) .career-listing-accordion-toggle-text-open').show();
      $(this).parent().siblings('.career-listing-body-inner').slideDown();
      $(this).find('.career-listing-accordion-toggle-text-close').show();
      $(this).find('.career-listing-accordion-toggle-text-open').hide();
    }
  });


  /*Career Listing Manual Toggle*/
  $('.accordion-manual-toggle .career-listing-accordion-toggle-inner').on('click', function() {
    $(this).closest('.career-listing-single').toggleClass('active');
    $(this).parent().siblings('.career-listing-body-inner').slideToggle();
    $(this).find('.career-listing-accordion-toggle-text-close').toggle();
    $(this).find('.career-listing-accordion-toggle-text-open').toggle();
  });


  /* Tabs */
  $('.tab-button').on('click', function() {
    if ( !$(this).hasClass('active') ) {
      $(this).closest('.tab-list').find('.tab-button').removeClass('active');
      $(this).addClass('active');

      var activeTab = $(this).data('tab');
      $(this).closest('.tabs').find('.tab-body-inner').not('#' + activeTab).hide();
      $('#' + activeTab).fadeIn();
      return false;
    }
  });

  /* Open First Tab on Page Load */
  $('.tab-button:first-child').addClass('active');
  $('.tab-body-inner:first-child').show();


  /* Cards Filter*/
  $('.cards-filter select').on('change', function(){
    var filter = $(this).find(':selected').attr('data-filter-button')

    if (filter == 'all') {
      $(this).closest('.cards-filter').siblings().children().show();
    } else {
      $(this).parent().siblings().children().hide();
      $(this).parent().siblings().children().each(function(){
        if ($(this).attr('data-filter-value').indexOf(filter) >= 0) {
          $(this).show();
        }
      });
    }
    
    $(this).parent().siblings('.gallery--masonry').masonry({
      itemSelector: '.cards-single'
    });
  });

  $('.cards-filter .button').on('click', function(){
    var filter = $(this).attr('data-filter-button');

    this.className = this.className.replace('button-outline-', 'button-');
    $(this).siblings().each(function() {
      if ( this.className.indexOf('-outline-') < 0) {
        this.className = this.className.replace('button-', 'button-outline-');
      }
    });

    if (filter == 'all') {
      $(this).parent().siblings().children().show();
    } else {
      $(this).parent().siblings().children().hide();
      $(this).parent().siblings().children().each(function(){
        if ($(this).attr('data-filter-value').indexOf(filter) >= 0) {
          $(this).show();
        }
      });
    }
    
    $(this).parent().siblings('.gallery--masonry').masonry({
      itemSelector: '.cards-single'
    });
  });


  /* Overlay Color */
  var overlayColorText = '';
  var overlayColorHex = '';
  var overlayBackground = '';
  $('.card-overlay').each(function(){
    overlayColorText = $(this).data('overlay-color');
    overlayColorHex = getComputedStyle(document.body).getPropertyValue('--color-' + overlayColorText);
    overlayBackground = 'linear-gradient(0deg,' + hexToSixDigit(overlayColorHex) + ' 30%, rgba(' + hexToRGB(hexToSixDigit(overlayColorHex)) + ', 0) 90% )';
    $(this).css('background', overlayBackground);
  });
  $('.gallery .card-overlay').each(function(){
    overlayColorText = $(this).data('overlay-color');
    overlayColorHex = getComputedStyle(document.body).getPropertyValue('--color-' + overlayColorText);
    overlayBackground = 'linear-gradient(0deg,' + hexToSixDigit(overlayColorHex) + ' 5%, rgba(' + hexToRGB(hexToSixDigit(overlayColorHex)) + ', 0.9) 15%, rgba(' + hexToRGB(hexToSixDigit(overlayColorHex)) + ', 0.75) 25%, rgba(' + hexToRGB(hexToSixDigit(overlayColorHex)) + ', 0) 65% )';
    $(this).css('background', overlayBackground);
  });
  $('.hero-banner-overlay').each(function(){
    overlayColorText = $(this).data('overlay-color');
    overlayColorHex = getComputedStyle(document.body).getPropertyValue('--color-' + overlayColorText);
    overlayBackground = 'rgba(' + hexToRGB(hexToSixDigit(overlayColorHex)) + ', 0.4)';
    $(this).css('background-color', overlayBackground);
  });
  $('.section-overlay').each(function(){
    overlayColorText = $(this).data('overlay-color');
    overlayColorHex = getComputedStyle(document.body).getPropertyValue('--color-' + overlayColorText);
    overlayBackground = 'rgba(' + hexToRGB(hexToSixDigit(overlayColorHex)) + ', 0.4)';
    $(this).css('background-color', overlayBackground);
  });
  $('.section-overlay-linear-gradient--top').each(function(){
    overlayColorText = $(this).data('overlay-color');
    overlayColorHex = getComputedStyle(document.body).getPropertyValue('--color-' + overlayColorText);
    overlayBackground = 'linear-gradient(180deg,' + hexToSixDigit(overlayColorHex) + ' 3px, rgba(' + hexToRGB(hexToSixDigit(overlayColorHex)) + ', 0) 100px )';
    $(this).css('background', overlayBackground);
  });
  $('.section-overlay-linear-gradient--bottom').each(function(){
    overlayColorText = $(this).data('overlay-color');
    overlayColorHex = getComputedStyle(document.body).getPropertyValue('--color-' + overlayColorText);
    overlayBackground = 'linear-gradient(0deg,' + hexToSixDigit(overlayColorHex) + ' 3px, rgba(' + hexToRGB(hexToSixDigit(overlayColorHex)) + ', 0) 100px )';
    $(this).css('background', overlayBackground);
  });


  /* Parallax Background */
  var parallaxLevel = -0.25; /* Change value to adjust parallax level */
  var parallaxBackgrounds = $('.bg-parallax');
  var parallaxElementOffsetTops = [];

  parallaxBackgrounds.each(function(i, el) {
    parallaxElementOffsetTops.push($(el).offset().top);
  });

  $(window).on('resize', function() {
    if ($(window).width() > 767) {
      parallaxElementOffsetTops = [];
      parallaxBackgrounds.each(function(i, el) {
        parallaxElementOffsetTops.push($(el).offset().top);
      });
    } else {
      parallaxBackgrounds.each(function(i, el) {
        $(el).removeAttr('style');
      });
    }
  });

  $(window).on('scroll', function() {
    if ($(window).width() > 767) {
      var scrollTop = $(this).scrollTop();
      parallaxBackgrounds.each(function(i, el) {
        var offsetTop = parallaxElementOffsetTops[i];
        $(el).attr('style', 'background-position-y: ' + (scrollTop - offsetTop) * parallaxLevel + 'px !important');
      });
    } else {
      parallaxBackgrounds.each(function(i, el) {
        $(el).removeAttr('style');
      });
    }
  });

  $(window).on('load', function(){
    /* Sticky Sidebar */
    if ($(window).width() > 767) {
      $('.sticky').each(function() {
        $(this).closest('.dnd-column').addClass('sticky-wrapper');
      });
      $('.sticky-wrapper').each(function() {
        $(this).stickySidebar({
          topSpacing: headerHeight + 20,
          bottomSpacing: 20
        });
      });
    }
  });


  /* Background Particles */
  if (!window.hsInEditor) {
    $('.section-background-particles').each(function() {
      var id = $(this).attr('id');
      var particleNumber = $(this).data('particle-number');
      var particleColorText = $(this).data('particle-color');
      var particleColorHex = getComputedStyle(document.body).getPropertyValue('--color-' + particleColorText);
      var particleShape = $(this).data('particle-shape');
      var particleSize = $(this).data('particle-size');
      var particleRandomSize = $(this).data('particle-random-particle-size');
      var particleOpacity = $(this).data('particle-opacity');
      var particleLineLink = $(this).data('particle-line-link');
      var particleMovementDirection = $(this).data('particle-movement-direction');
      var particleSpeed = $(this).data('particle-speed');
      var particleHoverEffect = $(this).data('particle-hover-effect');
      var particleHover = true;
      if (particleHoverEffect == 'none') {
        particleHover = false;
      }
      
      particlesJS(id, {
        "particles": {
          "number": {
            "value": particleNumber,
            "density": {
              "enable": true,
              "value_area": 800
            }
          },
          "color": {
            "value": hexToSixDigit(particleColorHex)
          },
          "shape": {
            "type": particleShape,
            "stroke": {
              "width": 0
            },
            "polygon": {
              "nb_sides": 5
            }
          },
          "opacity": {
            "value": particleOpacity,
            "random": false,
            "anim": {
              "enable": false,
              "speed": 1,
              "opacity_min": 0.01,
              "sync": false
            }
          },
          "size": {
            "value": particleSize,
            "random": particleRandomSize,
            "anim": {
              "enable": false,
              "speed": 40,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": particleLineLink,
            "distance": 150,
            "color": hexToSixDigit(particleColorHex),
            "opacity": 0.4,
            "width": 1
          },
          "move": {
            "enable": true,
            "speed": particleSpeed,
            "direction": particleMovementDirection,
            "random": false,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
              "enable": false,
              "rotateX": 600,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": particleHover,
              "mode": particleHoverEffect
            },
            "onclick": {
              "enable": false
            },
            "resize": true
          },
          "modes": {
            "grab": {
              "distance": 140,
              "line_linked": {
                "opacity": 1
              }
            },
            "bubble": {
              "distance": 400,
              "size": particleSize,
              "duration": 2,
              "opacity": 8,
              "speed": 3
            },
            "repulse": {
              "distance": 200,
              "duration": 0.4
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true
      });
    });
  }
});


/* Number Counter */
function numberCounter(el) {
  var $this = $(el).find('.counter-number-inner');
  $({ Counter: 0 }).animate({ 
    Counter: $this.attr('data-count') 
  },
  {
    duration: 2000,
    easing: 'linear',
    step: function () {
      $this.text(Math.floor(this.Counter).toLocaleString());
    },
    complete: function() {
      $this.text(this.Counter.toLocaleString());
    }
  });
};


/* Element Scrolled into View */
function isScrolledIntoView(el){
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var elTop = $(el).offset().top;
  var elBottom = elTop + $(el).height();
  return ((elBottom <= docViewBottom) && (elTop >= docViewTop));
}


/* Progress Bar - Line */
function progressBarLine(el, barNumberColor, barColorStart, barColorEnd, barColorBackground, barNumber) {
  var $this = $(el);
	var barID = $this.attr('id');
  
	var bar = new ProgressBar.Line('#' + barID, {
		strokeWidth: 50,
		easing: 'easeInOut',
		duration: 1500,
		color: barColorEnd,
		trailColor: barColorBackground,
		trailWidth: 50,
		from: {
      color: barColorStart,
    },
		to: {
      color: barColorEnd,
    },
		text: {
			style: {
				position: 'absolute',
				right: '0',
				top: '0',
        color: barNumberColor
			},
			autoStyleContainer: false
		},
		step: (state, bar) => {
      bar.path.setAttribute('stroke', state.color);
      bar.setText(Math.round(bar.value() * 100));
		},
		svgStyle: {
      width: '100%',
      height: '100%',
      'max-height': '30px'
    }
	});
	bar.animate(barNumber);
}


/* Progress Bar - Circle */
function progressBarCircle(el, barNumberColor, barColorStart, barColorEnd, barColorBackground, barNumber) {
  var $this = $(el);
	var barID = $this.attr('id');
    
	var bar = new ProgressBar.Circle('#'+barID, {
    strokeWidth: 16,
		easing: 'easeInOut',
		duration: 1500,
		color: barColorEnd,
		trailColor: barColorBackground,
		trailWidth: 16,
		from: {
      color: barColorStart,
    },
		to: {
      color: barColorEnd,
    },
		text: {
      style: {
        position: 'absolute',
        left: '50%',
        top: 'calc(50% - 0.625rem)',
        bottom: 'unset',
        color: barNumberColor,
        transform: 'translate(-50%, -50%)',
			},
			autoStyleContainer: true
		},
		step: function(state, bar) {
      bar.path.setAttribute('stroke', state.color);
      bar.setText(Math.round(bar.value() * 100));
		},
		svgStyle: {
      width: '100%',
      height: '100%',
      'max-width': '200px'
    }
	});
	bar.animate(barNumber);
}


/* Progress Bar - Semicircle */
function progressBarSemicircle(el, barNumberColor, barColorStart, barColorEnd, barColorBackground, barNumber) {
  var $this = $(el);
	var barID = $this.attr('id');
  
	var bar = new ProgressBar.SemiCircle('#'+barID, {
    strokeWidth: 16,
		easing: 'easeInOut',
		duration: 1500,
		color: barColorEnd,
		trailColor: barColorBackground,
		trailWidth: 16,
		from: {
      color: barColorStart,
    },
		to: {
      color: barColorEnd,
    },
		text: {
      style: {
        position: 'absolute',
        left: '50%',
        'padding-bottom': '1.75rem',
        color: barNumberColor,
			},
			autoStyleContainer: true
		},
		step: function(state, bar) {
      bar.path.setAttribute('stroke', state.color);
      bar.setText(Math.round(bar.value() * 100));
		},
		svgStyle: {
      width: '100%',
      height: '100%',
      'max-width': '200px'
    }
	});
	bar.animate(barNumber);
}


/* Convert Hex to RGB */
function hexToSixDigit(hex) {
  if (hex.length < 5) {
    hex = hex.replace(/([a-fA-F0-9])/g, '$1$1');
  }
  if (hex.charAt(0) !== '#') {
    hex = '#' + hex;
  }
  return hex;
}
function hexToRGB(hex) {
  hex = hex.replace('#', '');
  var r = parseInt(hex.substring(0, 2), 16);
  var g = parseInt(hex.substring(2, 4), 16);
  var b = parseInt(hex.substring(4, 6), 16);
  return r + ', ' + g + ', ' + b;
}